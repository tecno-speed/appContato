var CONTATOS = [
    {
        nome: 'anderson',
        email: 'anderson@servidor.com'
    },
    {
        nome: 'paula',
        email: 'paula@servidor.com'
    }
];

var INDEX_EDICAO = -1;

function excluir(i) {
    CONTATOS.splice(i, 1);
}

function editar(i) {
    INDEX_EDICAO = i;

    var contatoAtual = CONTATOS.filter(function (obj, iObj) {
        return iObj == i ? obj : null
    });

    document.getElementById('nome').value = contatoAtual[0].nome;
    document.getElementById('email').value = contatoAtual[0].email;
}

function salvarContato() {
    var nome = document.getElementById('nome').value;
    var email = document.getElementById('email').value;
    var valor = document.getElementById('valor').value;

    if (!nome || nome.length < 3) {
        alert('Preencha o nome corretamente')
        return false;
    }

    if (!email || email.length < 3) {
        alert('Preencha o e-mail corretamente')
        return false;
    }

    var contato = {
        nome: nome,
        email: email,
        valor: valor
    }

    if (INDEX_EDICAO < 0) {
        CONTATOS.push(contato)
    } else {
        CONTATOS[INDEX_EDICAO] = contato
        INDEX_EDICAO = -1;
    }

    alert('Salvo com sucesso!')

    document.getElementById('nome').value = '';
    document.getElementById('email').value = '';
}

MobileUI.validaValor = function(v) {
    if(v < 100) {
        return 'red'
    } else {
        return 'green'
    }
}
